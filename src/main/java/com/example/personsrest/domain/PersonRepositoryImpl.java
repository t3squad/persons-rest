package com.example.personsrest.domain;

import com.example.personsrest.exceptions.PersonNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.*;
import java.util.stream.Collectors;

public class PersonRepositoryImpl implements PersonRepository {

    Map<String, Person> personMap = new HashMap<>();

    @Override
    public Optional<Person> findById(String id) {
        return Optional.ofNullable(personMap.get(id));
    }

    @Override
    public List<Person> findAll() {
        return new ArrayList<>(personMap.values());
    }

    @Override
    public Page<Person> findAllByNameContainingOrCityContaining(String name, String city, Pageable pageable){

        List<Person> personList= personMap.values().stream().filter(x -> x.getName().equals(name) || x.getCity().equals(city)).collect(Collectors.toList());

        Page<Person> page = new PageImpl<>(personList);
        return page;
    }

    @Override
    public void deleteAll() {
        personMap.clear();
    }

    @Override
    public Person save(Person person) {
        personMap.put(person.getId(), person);
        return person;
    }

    @Override
    public void delete(String id) {
        personMap.remove(id);
    }
}
