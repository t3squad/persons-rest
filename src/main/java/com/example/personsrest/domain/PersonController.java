package com.example.personsrest.domain;

import com.example.personsrest.exceptions.GroupsNotExistException;
import com.example.personsrest.exceptions.PersonNotFoundException;
import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
@AllArgsConstructor
public class PersonController {

    PersonService personService;
    GroupRemote groupRemote;

    @GetMapping()
    public List<PersonDTO> all(@RequestParam(name = "search", required = false) String name,
                                        @RequestParam(name = "pagesize", defaultValue = "10") int size,
                                        @RequestParam(name = "pagenumber", defaultValue = "0") int number) {

        return personService.allOrBySearch(name, size, number).map(this::toDTO).collect(Collectors.toList());
    }

//    @GetMapping
//    public List<PersonDTO> all() {
//        return personService.all()
//                .map(this::toDTO)
//                .collect(Collectors.toList());
//    }
//
//    @GetMapping(params = {"search", "pagenumber", "pagesize"})
//    public List<PersonDTO> all(@RequestParam(value = "search", required = false) String name,
//                               @RequestParam(value = "pagesize", required = false) int size,
//                               @RequestParam(value = "pagenumber", required = false) int number) {
//
//        return personService.all(name, size, number).stream().map(this::toDTO).collect(Collectors.toList());
//    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDTO> get(@PathVariable("id") String id) {
        try {
            return ResponseEntity.ok(toDTO(personService.get(id)));

        } catch (PersonNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<PersonDTO> createPerson(@RequestBody CreatePersonDTO createPersonDTO) {
        try {
            return ResponseEntity.ok(toDTO(personService.createPerson(
                    createPersonDTO.getName(),
                    createPersonDTO.getCity(),
                    createPersonDTO.getAge())));

        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<PersonDTO> updatePerson(@PathVariable("id") String id, @RequestBody UpdatePersonDTO updatePersonDTO) {
        try {
            return ResponseEntity.ok(toDTO(personService.updatePerson(
                    id,
                    updatePersonDTO.getName(),
                    updatePersonDTO.getCity(),
                    updatePersonDTO.getAge()
            )));

        } catch (PersonNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePerson(@PathVariable("id") String id) {
        try {
            personService.deletePerson(id);
            return ResponseEntity.ok().build();
        } catch (PersonNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }


    //    @GetMapping("/{id}/addGroup/")
    @PutMapping("/{id}/addGroup/{groupName}")
    public ResponseEntity<PersonDTO> addGroup(@PathVariable("id") String id, @PathVariable("groupName") String groupName) {
        try {
            return ResponseEntity.ok(toDTO(personService.addGroup(
                    id,
                    groupName
            )));
        } catch (PersonNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    //    @GetMapping("/{id}/removeGroup/{groupId}")
    @DeleteMapping("/{id}/removeGroup/{groupName}")
    public ResponseEntity<PersonDTO> removeGroup(@PathVariable("id") String id, @PathVariable("groupName") String groupName) {
        try {
            return ResponseEntity.ok(toDTO(personService.removeGroup(
                    id,
                    groupName)));

        } catch (PersonNotFoundException e) {
            return ResponseEntity.notFound().build();
        } catch (GroupsNotExistException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    private PersonDTO toDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getCity(),
                person.getAge(),
                person.getGroups()
                        .stream()
                        .map(id -> groupRemote.getNameById(id))
                        .collect(Collectors.toList()));
    }

    @Value
    public static class PersonDTO {
        String id;
        String name;
        String city;
        int age;
        List<String> groups;
    }

    @Value
    public static class CreatePersonDTO {
        String name;
        String city;
        int age;
    }

    @Value
    static class UpdatePersonDTO {
        String name;
        String city;
        int age;
    }
}
