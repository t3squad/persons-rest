package com.example.personsrest.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonImpl implements Person {
    String id;

    String name;

    String city;

    int age;

    List<String> groups;

    boolean isActive;

    public PersonImpl(String name, String city, int age) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.city = city;
        this.age = age;
        this.groups = new ArrayList<>();
        this.isActive = true;
    }

    @Override
    public void addGroup(String groupId) {
        this.groups.add(groupId);
    }

    @Override
    public void removeGroup(String groupId) {
        this.groups.removeIf(group -> group.equals(groupId));
    }
}
