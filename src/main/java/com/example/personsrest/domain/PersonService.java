package com.example.personsrest.domain;

import com.example.personsrest.exceptions.GroupsNotExistException;
import com.example.personsrest.exceptions.PersonNotFoundException;
import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PersonService {

    PersonRepository personRepository;
    GroupRemote groupRemote;

    public Stream<Person> allOrBySearch(String search, int size, int number) {
        Pageable pageable = PageRequest.of(number, size);

        return search == null ?
                personRepository.findAll().stream() :
                personRepository.findAllByNameContainingOrCityContaining(search, search, pageable).stream();
    }

//    public Stream<Person> all() {
//        return personRepository.findAll().stream();
//    }
//
//    public List<Person> all(String search, int pagesize, int pagenumber) {
//        Pageable pageable = PageRequest.of(pagenumber, pagesize);
//        return personRepository.findAllByNameContainingOrCityContaining(search, search, pageable).toList();
//    }


    public Person get(String id) throws PersonNotFoundException {
        return personRepository.findById(id)
                .orElseThrow(() -> new PersonNotFoundException("Could not find person with id " + id));
    }

    public Person createPerson(String name, String city, int age) {
        return personRepository.save(new PersonImpl(
                name,
                city,
                age
        ));
    }

    public Person updatePerson(String id, String name, String city, int age) throws PersonNotFoundException {
        Person person = get(id);
        person.setName(name);
        person.setCity(city);
        person.setAge(age);
        return personRepository.save(person);
    }

    public void deletePerson(String id) throws PersonNotFoundException {
        personRepository.delete(id);
    }

    public Person addGroup(String id, String groupName) throws PersonNotFoundException {
        Person person = get(id);
        String groupId = groupRemote.createGroup(groupName);
        person.addGroup(groupId);
//        person.addGroupName(groupRemote.getNameById(groupId))     Spara både id & namn,
        return personRepository.save(person);
    }

    public Person removeGroup(String id, String groupName) throws PersonNotFoundException, GroupsNotExistException {
        Person person = get(id);

        if (person.getGroups().isEmpty()) {
            throw new GroupsNotExistException("Person with id " + id + " have no group");
        }

        for (String groupId: person.getGroups()) {              //        for (int i = 0; i < person.getGroups().size(); i++) {
            String group = groupRemote.getNameById(groupId);    //            String groupId = person.getGroups().get(i);
            if (group.contains(groupName)) {                    //            String group = groupRemote.getNameById(groupId);
                person.removeGroup(groupId);                    //            if (group.equals(groupName)){
                break;                                          //                person.removeGroup(groupId);
            }
        }

        return personRepository.save(person);
//            person.removeGroup(person.getGroups().get(0));            // Denna funkar med integrationstestet & vanliga testet
    }


}
