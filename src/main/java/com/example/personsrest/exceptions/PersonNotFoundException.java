package com.example.personsrest.exceptions;

public class PersonNotFoundException extends Exception {
    public PersonNotFoundException(String id){
        super(id);
    }
}
