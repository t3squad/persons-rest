package com.example.personsrest.exceptions;

public class GroupsNotExistException extends Exception {
    public GroupsNotExistException(String msg) {
        super(msg);
    }
}
